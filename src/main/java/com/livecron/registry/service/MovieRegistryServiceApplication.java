package com.livecron.registry.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class MovieRegistryServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieRegistryServiceApplication.class, args);
    }

}
